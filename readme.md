## Symbopy
Symbopy, es un Jupyter Notebook que muestra las propiedades de un **polinomio** ingresado por el usuario. Para alcanzar este resultado, Symbopy usa como backend a [Sympy](https://www.sympy.org/es/), el cual es una librería capaz de hacer matemática simbólica, lo que brinda la real potencia a Symbopy. Dado un polinomio las propiedades que este programa muestra son:

- Formas alternativas (Si están disponibles)

	- Simplificación
	- Factorización
	- Expansión

- Tipo de función
- Dominio
- Rango
- Derivada
- Integral
- Raices
- Puntos Críticos

Symbopy además puede ser extendido de forma bastante fácil (Véase la sección 3).

### 1. Guía de usuario
Para la instalación y su uso, debe disponer de [Jupyter Notebook o Jupyter Lab](https://jupyter.org/) en el entorno de Python donde va a ejecutar este programa. Para instalar Jupyter Notebook o Jupyter Notebook use el siguiente comando:
```bash
# Para instalar Jupyter Lab (incluye Jupyter Notebook)
pip install jupyterlab
```
Luego va abrir Jupyter y navegará donde a la carpeta donde clonó este repositorio con el comando:
```bash
# Jupyter lab 
jupyter lab
# Jupyter notebook
jupyter noteboook
```
Cuando se encuentre en la carpeta cree un nuevo cuaderno de Jupyter y en la primera celda e inserte el comando:
```bash
!pip install requirements.txt
```
Luego para ejecutar la celda va a presionar <kbd>Shift</kbd>+<kbd>Enter</kbd>. Este comando va a instalar las librerias necesarias para el funcionamiento del programa. Posteriormente, usted va a borrar esos comandos (se ejecutan solo la primera vez) y ira al menú superior y buscará la opción "Run.", despliegue el menú y seleccione la opción "Restart Kernel and Run All Cells". Si ve un cuadro de dialogo, ha hecho todo bien y está listo para usar Symbopy.

#### 1.1. Formato en que deben ser escritos los polinomios
El formato para escribir un polinomio en el cuadro de dialogo, es usando los operadores matemáticos de Python, si usted no sabe como usar los operadores de Python revise la siguiente tabla.

| **Operador** | **Significado**                     |
|--------------|-------------------------------------|
| `+`          | Suma                                |
| `-`          | Resta                               |
| `*`          | Multiplicación                      |
| `/`          | División                            |
| `()`         | Paréntesis para agrupar expresiones |
| `**`         | Potencia                            |
| `sqrt(x)`    | Raiz Cuadrada                       |

Las funciones pertenecientes al paquete **math** se ingresan sin el prefijo **math.**, por ejemplo la función `math.sin(x)` tiene que ser ingresada de la siguiente manera `sin(x)`. Para mas información sobre los operadores y funciones de la librería Math a continuación se presentan dos enlaces útiles: 
[math — Funciones matemáticas — documentación de Python - 3.10.0](https://docs.python.org/es/3/library/math.html)

[operator — Operadores estándar como funciones — documentación de Python - 3.10.0](https://docs.python.org/es/3/library/operator.html#mapping-operators-to-functions)


### 2. Funciones
Symbopy, es una plantilla extensible, es un poderoso motor capaz de ser extendido fácilmente con unos pocos ajustes. A pesar de **no** ser orientado a objetos, dadas las mismas pautas detrás de su creación, se puede extender su funcionalidad fácilmente. 
Las funciones preexistentes en Symbopy son las siguientes:

**input_expr**(_result_dict_)
Sirve para recoger la entrada proprocionada por el usuario, usa la función *input* de Python. Devuelve la entrada del usuario como una expresión de Sympy
> Argumentos
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario

> Valor de retorno
>  - **expr**:`Sympy.Add`: Es la expresión que ingresa el usuario en un objeto de Sympy

Uso:
```py
result_dict = {}
# Retona un objeto de Sympy
sympy_expr = input_expr(result_dict)
```
**expand_form**(_fy_, _result_dict_)
Expande el polinomio. En los casos que el usuario entregue un polinomio con la forma `(2*x+2)*(3*x+1)` el cual corresponde con un polinomio de grado 2, esta función agregará al diccionario *result_dict* la forma expandida del polinomio.
La forma expandida del polinomio se mostrará en la sección **formas alternativas**
> Argumentos
> - **fy**: Es el polinomio ya en forma de objeto sympy
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario

**simplify_form_xy**(_fy_, _result_dict_)
En caso de que la expresión sea simplificable, la función pondra en el diccionario *result_dict* la forma simplificada.
La forma simplificada se mostrará en la sección **formas alternativas**
> Argumentos
> - **fy**: Es el polinomio ya en forma de objeto sympy.
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

**factorization**(_fy_, _result_dict_)
Si la función es ingresada por el usuario en *forma expandida* (`2*x**3+4*x+6*x**2+50`) y esta dispone de una factorización incluye la forma factorizada en el diccionario *result_dict*.
 La forma factorizada se mostrará en la sección **formas alternativas**

**polynomial**(_fy_, _result_dict_)
Busca el tipo de función al que corresponde fy, y luego lo pone en el diccionario *result_dict*.
En adición la expresión de Sympy pasada como argumento, la retorna como un objeto `Sympy.Poly`
Esta se muestra en la sección **Propiedades**
> Argumentos
> - **fy**: Es el polinomio ya en forma de objeto sympy.
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

> Valor de retorno
> - **fy_poly`Sympy.Poly`**: Es la expresión de entrada como `Sympy.Poly(fy)`.

**extract_roots**(_fy_, _result_dict_)
Extrae las raíces reales e imaginarias y luego las pone en el diccionario de salida. El resultado se muestra en la sección **Raices**
> Argumentos
> - **fy**: Es el polinomio ya en forma de objeto sympy.
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

**is_constant**(_fy_, _result_dict_)
Si la función es constante la agrega al diccionario de resultados
> Argumentos
> - **fy**: Es el polinomio ya en forma de objeto sympy.
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

**recursive_dict_organization**(*key*, *result_dict*, *l*)
Es una utilidad para incluir una serie de elementos en una lista **ya existente** en el diccionario sin necesidad de crear otra. Esto se hace a través de una función recursiva.
> Argumentos
> - **key**: Es el elemento del diccionario donde se van a incluir los elementos `result_dict[key]`
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.
> - **l**: Es la lista desde la cual se toman los elementos 

Uso:
```py
from collections import defaultdict

result_dict = defaultdict(list)
elements = [1,2,3]

recursive_dict_organization("elementos", result_dict, elements)
```
**get_degree**(degree)
Es que recibe el grado de polinomio como numero entero, y lo devuelve como una cadena mas amigable con el usuario.
> Argumentos
> - **degree:`int`**: Un número entero de Python que corresponde al grado del polinomio

> Valor de retorno
> **degree_string:`str`**: Una cadena que almacena el grado del polinomio en forma de palabra en idioma español

**poly_kit**(*fy*, *fy_poly*, *result_dict*)
Empaqueta toda la información de la función y el polinomio tales como integral, derivada, grado, etc.

> Argumentos
> - **fy**: Es el polinomio ya en forma de objeto sympy.
> - **fy_poly**: Es el polinomio en forma `Sympy,Poly`
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

**analizer**(*fy*, *result_dict*):
Calcula y analiza los puntos criticos del polinomio entregado y los incluye en el diccionario *result_dict*.

> Argumentos
> - **fy**: Es el polinomio ya en forma de objeto sympy.
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

**clean_result**(default_dict)
Toma el diccionario de resultado y elimina algunas variables que no se limpian correctamente en los pasos anteriores 

> Argumentos
> - **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

**latex_label**(_label_, _content_)
Toma una string de Python, y un elemento del diccionario *result_dict* y retorna un string en formato **Latex**. Luego lo retorna como un objeto de tipo `IPython.display.Math`.
> Argumentos
> -   **result_dict**: Es un diccionario de Python. Este diccionario almacena todos los valores de que luego serán mostradas al usuario.

> Valores de retorno
> - **expresion_latex:`IPython.display.Math`**: Objeto para ser renderizado en Jupyter Notebook

**latex_format**(*content*, *style*)
Esta función recibe como argumento una parte del contenido del diccionario *result_dict* y un codigo de estilo. Estos dos argumentos pasan por una **maquina de estado finito** (FSM) (Vease la sección 4 y 5 para mas detalles) luego retorna una lista con los respectivos estilos aplicados al elemento en un formato entendible para la utilidad **display** de Jupyter Notebook. 
> Argumentos
> -   ** content**: Es un elemento del diccionario *result_dict*
> -  **style**: Un número entero (**int**) de Python que representa las funciones que se deben aplicar sobre el elemento
> Valores de retorno
> - **pack:`[IPython.display.Math]`**: Objeto para ser renderizado en Jupyter Notebook

**generate_element**(*title*, *content*, *style*, *gui*)
Toma un código de estilo (Vease la sección 4 y 5 para mas detalles), un titulo en forma de string de Python y una lista que contiene todos los elementos del GUI y retorna un objeto de tipo `Ipython.display.HTML`

> Argumentos
> -   **title:`str`**: Es un elemento del diccionario *result_dict*
> - **content**: Es un elemento del diccionario *result_dict*
> -  **style:`int`**: Un número entero (int) de Python que representa las funciones que se 
> - **gui:`list`**:  Es la lista con los demás elementos de la interfaz
> Valores de retorno
> - **pack:`[IPython.display.Math]`**: Objeto para ser renderizado en Jupyter Notebook


### 4. Detalles sobre el diseño extensible
Symbopy fue pensado como una fabrica, es decir hay funciones clave, que hacen a la vez como una especie de maquina. Como en toda fabrica hay códigos y comandos que solo pueden entender estas maquinas.

La cinta de montaje de Symbopy comienza tomando la expresión del usuario y encerrandola en un formato que las maquinas puedan entender, en este caso es un objeto de tipo *Sympy.add*, esto es gracias a la "bateria" de herramientas que nos provee Sympy para poder hacer funcionar a Symbopy, ya que provee de todas las funciones que son **bloques** de los que se alimenta este proyecto. 

Luego esta pasa a través de la cinta de montaje, la cual es extensible, donde cada expresión pasa por un proceso de 3 fases: 
	
- **Fase 1** Extracción de datos: Aquí es donde se ponen los datos a mostrar en el diccionario *result_dict*, estos pueden ser cualquier tipo de objeto, numeros, expresiones, graficos, dataframes, etc. 
- **Fase 2** Estilizado: Aquí es donde se le da un mejor aspecto visual y se monta la Interfaz de Usuario.
- **Fase 3** Mostrar los datos: Aquí es donde el proceso anterior termina mostrando el resultado al usuario de una forma agradable.

Esta hace a Symbopy mas que un simple programa estatico, un pequeño motor el cual se puede extender sin ni un problema a futuros, a pesar de que no este diseñado como un programa "orientado a objetos".

### 5. Extendiendo Symbopy: Symbopy 101

Symbopy, es fácilmente extensible, pero para poder extenderlo hay que entender los conceptos clave detrás de su diseño. Luego de ello crearemos un nuevo elemento de la interfaz siguiendo los mismos pasos.

#### 5.1 ¿Qué son los códigos de estilo?
Los códigos de estilo son numeros en **hexadecimal** que cobran mas sentido al ver su representación en código binario, por ejemplo el primer y segundo código:
```py
DO_NOTHING   = 0x1
PROPERTIES   = 0x2

# Representación en binario
DO_NOTHING = 0b01
PROPERTIES = 0b10
```
Si somos observadores nos habremos dado cuenta que todos dejan un *bit libre*, esto con el propósito de poder encadenar varios códigos de estilos con el operador lógico **or** (**|**):
```py
DO_NOTHING = 0b01
PROPERTIES = 0b10
# Por lo tanto
DO_NOTHING | PROPERTIES = 0b11
```
Esto hace que para poder comprobar los estilos podemos usar el operador lógico **y** (**&**), esto puede apreciarse en la función `latex_format`:
```py
if style & DO_NOTHING:
        pack.append(Math(sp.latex(content)))
if style & PROPERTIES:
            
        data = {
			...
```

Esta función se considera una *Maquina de estado finito* (FSM) ya que sigue el modelo de una cinta finita de instrucciones, y comprobaciones que desencadenan acciones. 

#### 5.2 ¿Cómo agregar una nueva función a Symbopy?
La respuesta a esta pregunta consta de dos pasos:

- Paso 1: Escribir una nueva función de Python: Esta función puede hacer **cualquier cosa**, lo importante es que siempre se retornen los valores **que va a ver el usuario final** hacia el diccionario *result_dlct*. Esto es porque será mas fácil luego agregar un nuevo elemento a la interfaz (Vease 5.3. ¿Como agregar un elemento a la interfaz de Symbopy?).

- Paso 2: Debe ponerla al final de la linea de montaje original de Symbopy, esta está identificada por un comentario, sin embargo se encuentra en la tercera celda del cuaderno original de Symbopy.

Agregar una nueva función a Symbopy es así de sencillo.

#### 5.3 ¿Como agregar un nuevo elemento?
Para agregar un nuevo elemento al GUI, debe efectuar los siguientes pasos:
- Paso 1: Cree un nuevo código de estilo, estos se encuentran en la primera celda del cuaderno
- Paso 2: agregar una condicional en la función `latex_format`, esta debe agregar un nuevo elemento al final de la lista `pack`
- Paso 3: En el fin de la línea de apartado visual cree una nueva linea de codigo que emplee la función `generate_element` con el titulo del apartado y el diccionario de resultado.

Luego de estos pasos usted ha agregado un nuevo elemento y una nueva función a Symbopy 

#### 5.3 Generando un elemento 'Hello World'
A continuación se detalla una pequeña guía para agregar un nuevo elemento a Symbopy. Primero vamos a crear una función que ponga un "_Hello World_" en nuestro diccionario:
```py
# La agregamos luego de la función generate_element
def hello_world(result_dict):
    result_dict['hello_world'] = "Hello World"
```
Ahora utilizaremos esa función al final de la linea de montaje
```py
# FIN DE LA LINEA DE MONTAJE
# lo agregamos luego del comentario
_ = hello_world(result_dict)
```

Ahora crearemos el nuevo elemento, primero vamos a crear un nuevo codigo de estilo, llamado `HELLO_WORLD` eso en mayúsculas debido a que es una constante:
```py
CRIT_POITNS  = 0x1000
# AGREGA AQUI NUEVOS ESTILOS
HELLO_WORLD  = 0x2000
```
Siempre debernos respetar la secuencia la cual va en múltiplos de 2. Ahora vamos a agregar una nueva función bajo la función `latex_format`, que le de a nuestro Hello World formato Latex
```py
    # AGREGA AQUI TU CONDICION
    # Este es nuestro nuevo trozo de codigo
    if style & HELLO_WORLD:
        if content['hello_world']:
            pack.append(Math(content['hello_world']))
    # Este es el retorno de la función
    return pack
```
Por ultimo agregamos una nueva llamada a la función `generate_element` la cual hará la magia
```py
# FIN DE LA LINEA DE APARTADO VISUAL
# Siempre agreguen el gui al final
generate_element('Hola mundo', result_dict, HELLO_WORLD, gui=gui) 
display(*gui)
```
En esos simples pasos ya has creado un nuevo el elemento el cual luce así:
![enter image description here](https://i.ibb.co/dKhSDXC/Resultado.png)
¡Solo queda divertirte!